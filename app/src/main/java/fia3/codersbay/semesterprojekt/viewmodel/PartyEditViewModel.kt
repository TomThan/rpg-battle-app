package fia3.codersbay.semesterprojekt.viewmodel;

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import fia3.codersbay.semesterprojekt.model.AbilityState
import fia3.codersbay.semesterprojekt.states.FightState
import fia3.codersbay.semesterprojekt.event.PartyEditEvent
import fia3.codersbay.semesterprojekt.model.Fia3Monster
import fia3.codersbay.semesterprojekt.model.Party
import fia3.codersbay.semesterprojekt.model.partyTestparty

class PartyEditViewModel() : ViewModel() {


    var _state = mutableStateOf(FightState(clickedActiveMonster = 0))
    val state: State<FightState> = _state


    fun onEvent2(event: PartyEditEvent) {
        if (!state.value.klicksperre) {
            when (event) {
                is PartyEditEvent.PartyEditEventClick -> {
                    _state.value = state.value.copy(
                        myAbilityState = AbilityState.PARTY
                    )
                }

                is PartyEditEvent.PartyEvent -> {
                    partyswitch(event.clickedPartyMember)
                }

                else -> {
                    println("d")
                }
            }
        }
    }

    fun partyswitch(partyMemberClicked: Fia3Monster) {
        _state.value = state.value.copy(
            monster = partyMemberClicked,
        )
        val removedPartyMemberIndex = partyTestparty.partyMemberList.indexOf(partyMemberClicked)
        if (removedPartyMemberIndex != -1) {
            partyTestparty.partyMemberList.drop(removedPartyMemberIndex)
        }
        _state.value = state.value.copy(
            partyState = partyTestparty.copy(
                partyMemberList = partyTestparty.partyMemberList
            )
        )

    }

    fun partyRemove(memberOfPartyClicked: Int) {

        _state.value = state.value.copy(
            partyState = state.value.partyState.copy(
                partyMemberList = state.value.partyState.partyMemberList.drop(memberOfPartyClicked)

            )

        )
    }

    fun partyAdd(memberOfPartyClicked: Fia3Monster) {

        _state.value = state.value.copy(
            partyState = state.value.partyState.copy(
                partyMemberList = state.value.partyState.partyMemberList.plus(memberOfPartyClicked)

            )
        )
    }
}




