package fia3.codersbay.semesterprojekt.viewmodel;

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fia3.codersbay.semesterprojekt.event.FightEvent
import fia3.codersbay.semesterprojekt.model.AbilityState
import fia3.codersbay.semesterprojekt.model.Attack
import fia3.codersbay.semesterprojekt.model.Fia3Monster
import fia3.codersbay.semesterprojekt.model.bossattacks
import fia3.codersbay.semesterprojekt.states.FightState
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class HomeViewModel() : ViewModel() {


    var _state = mutableStateOf(FightState(clickedActiveMonster = 0))
    val state: State<FightState> = _state

}


