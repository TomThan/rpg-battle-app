package fia3.codersbay.semesterprojekt.model

// Universal Attacks
val monsterenergy = Attack("Monster Energy","White Power Monster", -3000..-1000, false)
val epic7 = Attack("Epic Seven","Spiel Epic Seven heasd", 1..2999, false)


// Bartech Attacks
val pipi = Attack("pipiTSD","Bartech is suffering from PTSD", 300..500, false)
val pipu = Attack("Error Code 1337","StackOverFlow ist mein bester Freund", 300..500, false)
val pipa = Attack("Beste Marke","Ja Natürlich", 300..500, false)
val popelangriff = Attack("Log-in","Ich baue noch ein Log-in rein", 3000..4000, false)

val bartechattack = listOf<Attack>(pipi, pipu, pipa, popelangriff)

// Bartech Magic
val pipi2 = Attack("Monster Energy aus Polen","Monster Energy aus Polen", -4000..-2000, true)
val kappi2 = Attack("Polish Genes","Polish Anime Boy", 300..500, true)
val kappi3 = Attack("Polish Names","Grzegorz Brzęczyszczykiewicz", 300..500, true)

val bartechmagic = listOf<Attack>(pipi2, kappi2, kappi3, epic7)

// Oliver Atom Attacks

val topspin = Attack("Top Spin","Top Spin", 300..500, false)
val trainieren = Attack("McFit Combo","Geh ma trainieren, danach MCi", 600..1000, false)
val persona3 = Attack("Persona 3","Mitsuru ist best girl, was ist falsch mit dir", 300..500, false)
val nerd = Attack("Nerd","Mein Notendurchschnitt ist 1 komma ChatGPT", 300..500, false)

val oliattacks = listOf<Attack>(topspin, trainieren, persona3, nerd)

// Oliver Atom Magic

val optheory = Attack("Mr Morj","ich gucke täglich One Piece Theories", 300..500, true)
val willofD = Attack("Will of D","Mein Name ist actually Olli D. Kan ta ra", 300..500, true)

val olimagic = listOf<Attack>(optheory, willofD, epic7, monsterenergy)


// Atti The Hen Attacks
val scheisse = Attack("Shit","Alles ist scheisse mann", 300..500, false)
val seufz = Attack("But","Aber......... seufz", 300..500, false)
val fia2 = Attack("FIA2","Ich gehöre eigentlich zur Fia 2", 300..500, false)


val attiattacks = listOf<Attack>(scheisse, seufz, fia2)

// Atti The Hen Magic
val eint = Attack("ein T","Atti mit einem T", 300..500, false)

val attimagic = listOf<Attack>(eint,epic7)


// Fox McKaut Attacks
val hiya = Attack("Flip Kick", "Hiya",300..500, false)
val hiya2 = Attack("Fox Split", "Hiya", 300..500, false)
val hiya3 = Attack("Roundhouse Kick","Hiya" , 300..500, false)
val hiya4 = Attack("Taunt","Come on" , 0..0, false)

val floattacks = listOf<Attack>(hiya,hiya2,hiya3,hiya4)

// Fox McKaut Magic
val hiya5 = Attack("Fox Fire", "Hiya" ,300..500,true)
val hiya6 = Attack("Reflector", "Hiya" ,300..500,true)
val hiya7 = Attack("Blaster", "Hiya" ,300..500,true)

val flomagic = listOf<Attack>(hiya5,hiya6,hiya7, monsterenergy)



val bossattacks = listOf<Attack>(
    Attack("Kratzen","Test", 10..11, isMagic = false),
    Attack("Bieber", "Test mit ur viel Text",10..11, isMagic = false),
    Attack("Bibelaa", "Test mir noch mehr viel Text oida",100..200, isMagic = false),
)

val bossattacks2 = listOf<Attack>(
    Attack("Fireball", "Fireball",100..200, isMagic = true),
    Attack("Blizzard","what did you just say to me you little b", 300..400, isMagic = true),
    Attack("Fabel", "once upon a time, there was a sailor who has ", 10..11, isMagic = true),
    Attack("Bibelaa","ppppppppppppppppppppppppppppppppppp", 100..200, isMagic = true)
)


val attack1 = Attack("Monster Energy","pipi", 300..500, false)
val attack2 = Attack("PimPim","popoapm", 300..500, false)
val attack3 = Attack("PamPam","kdkdkdkdk", 300..500, false)
val popelangriff2 = Attack("Popel Angriff 2.0","dddddddddddddddd", 3000..4000, false)


// Magic
val fire = Attack("Fire","Fire", 400..700, isMagic = true)
val ice = Attack("Ice","Ice", 400..700, isMagic = true)
val thunder = Attack("Thunder", "Thunder",400..700, isMagic = true)
val heal = Attack("Cure","Cure", -600..-500, isMagic = true)

val magicList = listOf<Attack>(fire, ice, thunder, heal)


///////// Hard Coded Boss Data //////////////

//Alex Boss Attack
val hoiste = Attack("Hoisting","Heute lernen wir über State Hoisting", 400..700, false)
val fish = Attack("Neue Klasse","Hier ist eine Data Class Fish", 400..700, false)
val david = Attack("David","Die Folien hat der David gemacht", 500..1000, false)

val Dragon1Attack = listOf<Attack>(hoiste, fish, david)

//Alex Boss 2
val urlaub = Attack("Urlaub","Ich mache jetzt 2 Wochen Urlaub", 400..700, false)
val lambda = Attack("Lambda","Hier kommt noch eine Lambda Funktion rein", 400..700, false)
val insert = Attack("insert","Erinnert Ihr euch noch an Datenbanken?", 400..700, false)

val Dragon2Attack = listOf<Attack>(urlaub, lambda, insert)

//Stefan Boss 1
val deflection = Attack("deflect","Die Aufgaben sind vom Alex", 400..700, false)
val algorithm2 = Attack("nlogn","nlognhoch2", 400..700, false)
val ameisen = Attack("Ant","Antgame Buffed Dummy Bots", 400..700, false)

val angel1 = listOf<Attack>(deflection, algorithm2, ameisen)

//Stefan Boss 2
val rekursion = Attack("Rekursion","Rekursion", 400..700, false)

val angel2 = listOf<Attack>(rekursion)

//Roman Boss 1
val abwesend = Attack("Stroll through Hell","You hear Screams while going through Hell", 0..0, false)

val demon1 = listOf<Attack>(abwesend)

//Roman Boss 2
val delegatation = Attack("Delegate","Delegates", 400..700, false)
val exposition = Attack("Exposed","Exposed", 400..700, false)
val composition = Attack("Compose","Dependancy", 400..700, false)

val demon2 = listOf<Attack>(delegatation, exposition, composition)