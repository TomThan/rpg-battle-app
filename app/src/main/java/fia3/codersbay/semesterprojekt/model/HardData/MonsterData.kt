package fia3.codersbay.semesterprojekt.model

val olli = Fia3Monster("Bulliver", oliattacks, olimagic, 20500, 20500)
val bartech = Fia3Monster("Bärtech", bartechattack, bartechmagic, 20500, 20500)
val oliver = Fia3Monster("Oliver Atom",oliattacks, olimagic, 20500, 20500)
val ati = Fia3Monster("Ati The Hen", attiattacks, attimagic, 20500, 20500)
val flohx = Fia3Monster("Fox McKaut", floattacks , flomagic, 20500, 20500)
val pig5 = Fia3Monster("Porte5k", listOf<Attack>(pipi, pipu, pipa, popelangriff), magicList, 500, 500)


val bossdragon1 = Fia3Monster("Dragon", Dragon1Attack, bossattacks2,10000,10000)
val bossdragon2 = Fia3Monster("Hydra", Dragon2Attack, bossattacks2, 4999,10000)

val angel = Fia3Monster("Sehtim, The Mean Steam Machine",angel1, bossattacks2, 10000,10000)
val fallenangel = Fia3Monster("Sehtim, The Alpha and The Omega", angel2, bossattacks2, 4999, 10000)

val roman = Fia3Monster("Hellscape", demon1, bossattacks2, 10000, 10000)
val roman2 = Fia3Monster("Demon", demon2, bossattacks2, 4999, 10000)



val boss1 = Fia3Monster(
    name = "Boss",
    bossattacks,
    bossattacks2,
    4000,
    4000,
)

val boss2 = Fia3Monster(
    name = "Boss2",
    bossattacks2,
    bossattacks,
    3000,
    5000,
)

val partyTestparty = Party(partyMemberList =
listOf(bartech.copy(),
    olli.copy(),
    boss1.copy(),
    oliver.copy(),
    ati.copy(),
    flohx.copy(),
    pig5.copy(),
    boss2.copy()
)
)