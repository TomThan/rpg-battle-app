package fia3.codersbay.semesterprojekt.model

data class Attack(val name: String, val description:String, val dmg: IntRange, val isMagic: Boolean) {
    fun getAttackImageID(): Int? {
        return imageMapofAttack[name]
    }
}


//data class TextEvent(val text:String, val eventTrigger: (Boolean, Fia3Monster, Fia3Monster) -> Boolean)

data class Party(val partyMemberList: List<Fia3Monster>)

data class Fia3Monster(
    val name: String,
    val AttackNames: List<Attack>,
    val MagicNames: List<Attack>,
    var currentHealth: Int,
    val maxHealth: Int,
) {
    fun getImageID(): Int? {
        return imageloaderMap[name]
    }
}
