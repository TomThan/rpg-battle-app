package fia3.codersbay.semesterprojekt.database

import fia3.codersbay.semesterprojekt.database.model.dao.Attack
import fia3.codersbay.semesterprojekt.database.model.dao.FightCounter
import fia3.codersbay.semesterprojekt.database.model.schema.Attacks
import fia3.codersbay.semesterprojekt.database.model.schema.FightCounters
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.DatabaseConfig
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction

fun configureDatabase() {
    Database.connect(
        url ="jdbc:h2:/data/data/fia3.codersbay.semesterprojekt/data/db;DB_CLOSE_DELAY=-1",
        driver = "org.h2.Driver",
        databaseConfig = DatabaseConfig {
            this.keepLoadedReferencesOutOfTransaction = true
        }

    )

    transaction {
        SchemaUtils.createMissingTablesAndColumns(FightCounters)
        FightCounter.new {
            name = "Battles done"
            Counter = 0
        }
//        FightCounter.new {
//            name = "xp gained"
//            Counter = 0
//        }
    }
}