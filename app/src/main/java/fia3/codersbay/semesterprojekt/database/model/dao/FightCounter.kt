package fia3.codersbay.semesterprojekt.database.model.dao

import fia3.codersbay.semesterprojekt.database.model.schema.FightCounters
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class FightCounter(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<FightCounter>(FightCounters)

    var name by FightCounters.name
    var Counter by FightCounters.Counter

}