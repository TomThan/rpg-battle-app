package fia3.codersbay.semesterprojekt.database.model.dao

import fia3.codersbay.semesterprojekt.database.model.schema.Attacks
import fia3.codersbay.semesterprojekt.model.imageMapofAttack
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class Attack(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Attack>(Attacks)

    var name by Attacks.name
    private var min_dmg by Attacks.min_dmg
    private var max_dmg by Attacks.max_dmg
    var dmg
        get() = min_dmg..max_dmg
        set(value) {
            min_dmg = value.first
            max_dmg = value.last
        }
    var isMagic by Attacks.isMagic

    fun getAttackImageID(): Int? {
        return imageMapofAttack[name]
    }
}