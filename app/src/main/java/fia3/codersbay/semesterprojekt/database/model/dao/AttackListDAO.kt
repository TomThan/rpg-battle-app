//package fia3.codersbay.semesterprojekt.database.model.schema
//
//import fia3.codersbay.semesterprojekt.database.model.schema.AttackList.bool
//import fia3.codersbay.semesterprojekt.database.model.schema.AttackList.varchar
//import org.jetbrains.exposed.dao.IntEntity
//import org.jetbrains.exposed.dao.IntEntityClass
//import org.jetbrains.exposed.dao.UUIDEntity
//import org.jetbrains.exposed.dao.UUIDEntityClass
//import org.jetbrains.exposed.dao.id.EntityID
//import org.jetbrains.exposed.dao.id.IntIdTable
//import java.util.UUID
//
//class AttackListDAO(id: EntityID<Int>) : IntEntity(id) {
//    companion object : IntEntityClass<AttackListDAO>(AttackList)
//
//    var name by AttackList.name
//    var attack1 = Attacks referencedOn AttackList.attack1
//    var attack2 = Attacks referencedOn AttackList.attack2
//    var attack3 = Attacks referencedOn AttackList.attack3
//    var attack4 = Attacks referencedOn AttackList.attack4
//}