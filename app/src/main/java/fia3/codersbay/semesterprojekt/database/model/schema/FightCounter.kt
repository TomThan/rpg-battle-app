package fia3.codersbay.semesterprojekt.database.model.schema

import org.jetbrains.exposed.dao.id.IntIdTable

object FightCounters : IntIdTable() {

    var name = varchar("name", 255)
    var Counter = integer("counter")

}