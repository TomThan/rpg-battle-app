package fia3.codersbay.semesterprojekt.states

import fia3.codersbay.semesterprojekt.model.AbilityState
import fia3.codersbay.semesterprojekt.model.Attack
import fia3.codersbay.semesterprojekt.model.Fia3Monster
import fia3.codersbay.semesterprojekt.model.*

data class FightState(
    var enemytakendamage: Int? = null,
    var enemyMonster: Fia3Monster = boss1.copy(),
//	val myAttack: Attack = Attack("Nix",300..400),
//	val myMagic: Magic = Magic("Fix", 300 .. 400),
    var myAbilityState: AbilityState = AbilityState.PARTY,
    var klicksperre: Boolean = false,
    var attackAnimation: Boolean = false,
    var enemyAttackAnimation: Boolean = false,
    var takendamage: Int? = null,
    var attackUsed: Attack? = null,
    var bossattackUsed: Attack? = null,
    val partyState: Party = Party(partyMemberList = listOf(
        bartech.copy(),
        oliver.copy(),
        ati.copy(),
        flohx.copy())),
    var monster: Fia3Monster = partyState.partyMemberList[0],  /*komm später aus db*/
    val victoryState: Boolean = false,
    val defeatState: Boolean = false,
    val enterBattleState: Boolean = false,
    var defendState: Boolean = false,

    var clickedActiveMonster: Int

//	val magicList: List<Magic> = listOf<Magic>(fire, ice, thunder),

//	val pupu: List<Attack> = listOf<Attack>(pipi, pipu, pipa),
) {
//    init {            Setzte den State dadurch immer auf den Init State
//        monster = partyState.partyMemberList[0].copy()
//    }
}