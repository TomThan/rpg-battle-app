package fia3.codersbay.semesterprojekt.event

import fia3.codersbay.semesterprojekt.model.Fia3Monster

sealed class PartyEditEvent {


    object PartyEditEventClick : PartyEditEvent()

    object PartyMemberEvent : PartyEditEvent()

    data class PartyEvent(var clickedPartyMember: Fia3Monster) : PartyEditEvent()

}
