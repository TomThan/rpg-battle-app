package fia3.codersbay.semesterprojekt.screen.Buttons

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyHorizontalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import fia3.codersbay.semesterprojekt.model.Fia3Monster
import fia3.codersbay.semesterprojekt.model.Party
import fia3.codersbay.semesterprojekt.model.partyTestparty
import fia3.codersbay.semesterprojekt.screen.LifeundTopBar.LifeBar
import java.time.format.TextStyle


@Composable
fun PartyButton(
    partyMember: Fia3Monster,
    partyIndex: Int,
    onClick: (Fia3Monster) -> Unit,
    klicksperre: Boolean
) {
    Box(modifier = Modifier
        .clickable {
            onClick(partyMember)
        }
        .width(140.dp)
        .background(if (!klicksperre) Color.Green else Color.Green.copy(alpha = 0.5f))) {
        Column() {
            Box(
                modifier = Modifier
                    .height(20.dp)
                    .fillMaxWidth()
                    .background(color = Color.White)
                    .padding(start = 40.dp)
            ) {
                Text(text = partyMember.name, textAlign = TextAlign.Justify, style = MaterialTheme.typography.bodyLarge)
            }
//            if (party.partyMemberList.size >= partyIndex) {
                partyMember.getImageID()?.let {
                    Image(
                        painter = painterResource(id = it),
                        contentDescription = "T",
                        contentScale = ContentScale.Crop,
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(90.dp)
                    )
//                }
            }
            LifeBar(
                life = partyMember.currentHealth,
                MaxLife = partyMember.maxHealth,
                modifier = Modifier
                    .padding(start = 0.dp, top = 0.dp,)
            )
        }
    }
}

@Composable
fun PartyMenu(onPartySwitched: (Fia3Monster) -> Unit, klicksperre: Boolean, partyState: Party) {
    LazyHorizontalGrid(
        rows = GridCells.Fixed(2),
        contentPadding = PaddingValues(1.dp),
        horizontalArrangement = Arrangement.spacedBy(5.dp),
        verticalArrangement = Arrangement.spacedBy(5.dp)
    ) {
        itemsIndexed(partyState.partyMemberList) { index, partyMember ->
            PartyButton(partyMember = partyMember, partyIndex = index, onClick ={onPartySwitched(it)} , klicksperre = klicksperre
            )
        }
    }
}
