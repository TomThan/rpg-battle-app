package fia3.codersbay.semesterprojekt.screen.Buttons

import android.graphics.fonts.Font
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyHorizontalGrid
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import fia3.codersbay.semesterprojekt.R
import fia3.codersbay.semesterprojekt.model.Attack
import fia3.codersbay.semesterprojekt.model.Fia3Monster

@Composable
fun AttackButton(attack: Attack, onClick: (Attack) -> Unit, klicksperre: Boolean) {
    Box(modifier = Modifier
        .clickable {
            onClick(attack)
            //Es macht damage von einer range
            //Nach dem drücken ausgegraut
            //Text ausgeben wie viel damage gemacht wurde
            //Diese Int range von der Health des Gegners abziehen
            //Enemy Turn Funktion
        }
        .height(120.dp)
        .width(140.dp)
//        .background(if (!klicksperre) Color.Green else Color.Green.copy(alpha = 0.5f))
    )
    {


        Image(
            modifier = Modifier
                .fillMaxSize(),
            painter = painterResource(
                id = if (!klicksperre) R.drawable.actionbutton
                else R.drawable.actionbuttondisabled
            ), contentDescription = "GBF Button",
            contentScale = ContentScale.Crop
        )

        Text(
            modifier = Modifier
                .padding(start = 20.dp, top = 30.dp),
            text = attack.name,
            color = Color.Black,
            maxLines = 2,
            overflow = TextOverflow.Ellipsis,
            style = TextStyle(fontSize = 20.sp, fontWeight = FontWeight.Bold)
        )
        Text(
            modifier = Modifier
                .padding(start = 10.dp, top = 100.dp),
            text = "Damage: ${attack.dmg}",
            color = Color.Black,
            style = TextStyle(fontSize = 15.sp)
        )
    }
}

@Composable
fun AttackMenu(
    onAttackPerformed: (Attack) -> Unit,
    klicksperre: Boolean,
    activeFia3Monster: Fia3Monster
) {
    LazyHorizontalGrid(
        rows = GridCells.Fixed(2),
        contentPadding = PaddingValues(1.dp),
        horizontalArrangement = Arrangement.spacedBy(5.dp),
        verticalArrangement = Arrangement.spacedBy(5.dp)
    ) {
        items(activeFia3Monster.AttackNames) { attack ->
            AttackButton(
                attack = attack,
                onClick = { onAttackPerformed(it) },
                klicksperre = klicksperre
            )
        }

    }
}
