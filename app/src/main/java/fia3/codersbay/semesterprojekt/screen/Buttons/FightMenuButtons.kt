package fia3.codersbay.semesterprojekt.screen.Buttons

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.unit.dp

@Composable
fun MenuButtonsFightScreen(
    textvariable: String,
    onClickvariable: () -> Unit,
    buttonImageid: Int,
    activeButton: Boolean = false,
) {
    Button(
        modifier = Modifier
            .height(70.dp)
            .width(100.dp),
        enabled = !activeButton,
        onClick = { onClickvariable() },
        colors = ButtonDefaults.buttonColors(
            disabledContentColor = Color.Gray,
            disabledContainerColor = Color.Blue
        ),
        contentPadding = PaddingValues(0.dp, 0.dp, 0.dp, 0.dp),
        shape = RectangleShape
    )
    {
        Icon(imageVector = Icons.Filled.Edit, contentDescription = "Edit")
        Text(text = textvariable, style = MaterialTheme.typography.bodyLarge)
    }
}
