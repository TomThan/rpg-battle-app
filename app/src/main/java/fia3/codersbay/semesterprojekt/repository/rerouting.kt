package fia3.codersbay.semesterprojekt.repository

sealed class rerouting(val route: String) {
    object FightScreen: rerouting("fight_screen")
    object PartyEditScreen: rerouting("party_screen")
    object HomeScreen: rerouting("home_screen")
}