package fia3.codersbay.semesterprojekt.event

import fia3.codersbay.semesterprojekt.database.model.schema.PartySlots
import fia3.codersbay.semesterprojekt.model.Fia3Monster

sealed class PartyEditEvent {

    data class PartyEvent(var clickedPartyMember: Fia3Monster) : PartyEditEvent()

    data class PartyEvent2(var clickedPartyMember: Fia3Monster) : PartyEditEvent()

    data class PartyEventtoDB(val PartyMember: List<PartySlots> = emptyList()) : PartyEditEvent()
}
