package fia3.codersbay.semesterprojekt.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.lifecycle.viewmodel.compose.viewModel
import fia3.codersbay.semesterprojekt.R
import fia3.codersbay.semesterprojekt.database.model.dao.FightCounter
import fia3.codersbay.semesterprojekt.database.model.schema.FightCounters
import fia3.codersbay.semesterprojekt.repository.rerouting
import fia3.codersbay.semesterprojekt.viewmodel.HomeViewModel
import org.jetbrains.exposed.sql.transactions.transaction


@Composable
fun HomeScreen(
    navController: NavController,
    viewModel: HomeViewModel = viewModel(),
    navigateTo: () -> Unit,
) {


    Box(
        contentAlignment = Alignment.CenterStart,
        modifier = Modifier
            .fillMaxSize()

    ) {
        Image(
            painter = painterResource(id = R.drawable.rpgbackground),
            contentDescription = "Background",
            modifier = Modifier
                .fillMaxSize()
        )
        Box(
            modifier = Modifier
                .padding(start = 50.dp, top = 50.dp)
        )
        {
            Column(
            ) {
                Image(
                    painter = painterResource(id = R.drawable.codersbay),
                    contentDescription = "Background",
                    modifier = Modifier
                        .padding(start = 65.dp)
                        .size(150.dp)
                )
                MenuButtons(
                    "Fight1",
                    onClick = { navController.navigate(route = "FightScreen" + "?level=1") },
                    buttonId = R.drawable.choosebutton,
                    logoId = R.drawable.android_logo
                )
                MenuButtons(
                    "Fight2",
                    onClick = { navController.navigate(route = "FightScreen" + "?level=3") },
                    buttonId = R.drawable.choosebutton,
                    logoId = R.drawable.kotlin
                )
                MenuButtons(
                    "Fight3",
                    onClick = { navController.navigate(route = "FightScreen" + "?level=5") },
                    buttonId = R.drawable.choosebutton,
                    logoId = R.drawable.java
                )
//                MenuButtons("Fight4", buttonId = R.drawable.choosebutton, logoId = R.drawable.datenbank)
                MenuButtons(
                    "Edit Team",
                    onClick = { navController.navigate(route = rerouting.PartyEditScreen.route) },
                    buttonId = R.drawable.team_button,
                    logoId = R.drawable.team_logo
                )
                Text(
                    modifier = Modifier
                        .background(Color.White),
                    text = "Battles won: " + transaction {
                        FightCounter.find { FightCounters.name eq "Battles done" }.first()
                    }.Counter.toString()
                )
            }
        }
    }
}


@Composable
fun MenuButtons(
    text: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    buttonId: Int,
    logoId: Int

) {
    Box(
        contentAlignment = Alignment.CenterStart,
        modifier = modifier
            .clickable { onClick() }
            .width(width = 283.dp)
            .height(height = 62.dp)


    ) {
        Image(
            painter = painterResource(buttonId),
            contentDescription = "Boss Button",
            modifier = Modifier
                .fillMaxSize()
        )
        Row(
            modifier = Modifier
                .padding(start = 40.dp, top = 4.dp)
        ) {

            Image(
                painter = painterResource(id = logoId),
                contentDescription = "Team Logo",
                modifier = Modifier
                    .padding(top = 10.dp, end = 9.dp)
                    .width(width = 35.dp)
                    .height(height = 37.dp)
            )
            Text(
                text = text,
                color = Color(0xfff5f5f5),
                style = androidx.compose.ui.text.TextStyle(
                    fontSize = 34.sp,
                    fontWeight = FontWeight.Bold
                ),
                modifier = Modifier
                    .width(width = 190.dp)
                    .height(height = 72.dp)
            )
        }
    }
    Box(
        modifier = Modifier
            .padding(start = 50.dp, top = 50.dp)
    )
}