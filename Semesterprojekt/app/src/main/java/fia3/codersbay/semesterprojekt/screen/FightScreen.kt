package fia3.codersbay.semesterprojekt.screen

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import fia3.codersbay.semesterprojekt.viewmodel.FightViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import fia3.codersbay.semesterprojekt.R
import fia3.codersbay.semesterprojekt.event.FightEvent
import fia3.codersbay.semesterprojekt.model.AbilityState
import fia3.codersbay.semesterprojekt.model.Attack
import fia3.codersbay.semesterprojekt.model.Fia3Monster
import fia3.codersbay.semesterprojekt.model.Party
import fia3.codersbay.semesterprojekt.screen.Buttons.AttackMenu
import fia3.codersbay.semesterprojekt.screen.Buttons.MagicMenu
import fia3.codersbay.semesterprojekt.screen.Buttons.MenuButtonsFightScreen
import fia3.codersbay.semesterprojekt.screen.Buttons.PartyMenu
import fia3.codersbay.semesterprojekt.screen.Buttons.VictoryButton
import fia3.codersbay.semesterprojekt.screen.LifeundTopBar.BigBlackMiddleBar
import fia3.codersbay.semesterprojekt.screen.LifeundTopBar.BigBlackTopBar
import fia3.codersbay.semesterprojekt.screen.LifeundTopBar.LifeBar


@Composable
fun FightScreen(
    navController: NavController,
    viewModel: FightViewModel = viewModel(),
    navigateTo: () -> Unit,
) {


    val state = viewModel.state
//    val AbilityState = viewModel.abilityState


    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Blue),
    )
    {
        Column() {
//            AnimatedVisibility(
//                visible = state.value.klicksperre,
//                enter = slideInHorizontally(tween(1000)),
//                exit = fadeOut(tween(1000))
//            )
//            {

//            }
////            if (state.value.klicksperre) {
            Box(
                modifier = Modifier
                    .height(60.dp)
                    .fillMaxWidth()
                    .background(Color.Black)

            ) {
                BigBlackTopBar(

                    text = if (state.value.enemytakendamage != null) {
                        "${state.value.enemyMonster.name} has taken ${state.value.enemytakendamage} damage"
                    } else if (state.value.takendamage != null) {
                        "${state.value.monster.name} has taken ${state.value.takendamage} damage"
                    } else {
                        ""
                    }
                )
            }

//            }
            Box(
                modifier = Modifier
                    .height(300.dp)
                    .fillMaxWidth()
                    .background(Color.Black),
            )
            {
                state.value.enemyMonster.getImageID()?.let {
                    Image(
                        painter = painterResource(id = it),
                        contentDescription = "T"
                    )
                }

                LifeBar(
                    state.value.enemyMonster.currentHealth, state.value.enemyMonster.maxHealth,
                    modifier = Modifier
                        .padding(start = 50.dp, top = 10.dp)
                )

                BigBlackMiddleBar(
                    text = if (state.value.enemytakendamage != null) {
                        "${state.value.attackUsed?.description}"
                    } else if (state.value.defendState) {
                        "${state.value.monster.name} defends!"
                    } else if (state.value.takendamage != null) {
                        "${state.value.bossattackUsed?.description}"
                    } else {
                        ""
                    }
                )

                /***********************        Animation Block Start            **********************/
                Column() {
                    AnimatedVisibility(
                        visible = state.value.attackAnimation,
                        enter = fadeIn(tween(100)),
                        exit = fadeOut(tween(250))
                    ) {
                        Box(
                            modifier = Modifier
                                .padding(start = 120.dp, top = 80.dp)
                                .size(150.dp)
                        ) {
                            state.value.attackUsed?.let {

                                val index = state.value.monster.AttackNames.indexOf(it)
                                val index2 = state.value.monster.MagicNames.indexOf(it)
                                if (index >= 0) {
                                    state.value.monster.AttackNames[index].getAttackImageID()
                                        ?.let { id ->
                                            Image(
                                                painter = painterResource(id = id),
                                                contentDescription = "Attack Animation",
                                                contentScale = ContentScale.Crop
                                            )
                                        }
                                }
                                if (index2 >= 0) {
                                    state.value.monster.MagicNames[index2].getAttackImageID()
                                        ?.let { id ->
                                            Image(
                                                painter = painterResource(id = id),
                                                contentDescription = "Magic Animation"
                                            )
                                        }
                                }
                            }
                        }
                    }
                    AnimatedVisibility(
                        visible = state.value.enemyAttackAnimation,
                        enter = fadeIn(tween(1)),
                        exit = fadeOut(tween(300))
                    ) {
                        Box(
                            modifier = Modifier
                                .padding(start = 140.dp, top = 30.dp)
                                .size(150.dp)
                        ) {
                            state.value.bossattackUsed?.let {
                                val index3 = state.value.enemyMonster.AttackNames.indexOf(it)
                                if (index3 >= 0) {
                                    state.value.enemyMonster.AttackNames[index3].getAttackImageID()
                                        ?.let { id ->
                                            Image(
                                                painter = painterResource(id = id),
                                                contentDescription = "Boss Attack Animation"
                                            )
                                        }
                                }
                            }
                        }
                    }

                    AnimatedVisibility(
                        visible = state.value.victoryState,
                        enter = fadeIn(tween(1)),
                        exit = fadeOut(tween(300))
                    ) {
                        VictoryButton(

                            "You have won the Battle against " + "\n" +
                                    "${state.value.enemyMonster.name}!",
                            onClick = {
                                FightEvent.ResetHealthEvent
                                navController.navigate(route = "HomeScreen")
                            },
                        )
                    }
                    AnimatedVisibility(
                        visible = state.value.defeatState,
                        enter = fadeIn(tween(1)),
                        exit = fadeOut(tween(300)),
                    ) {
                        VictoryButton(
                            "You have lost the Battle against " + "\n" +
                                    "${state.value.enemyMonster.name}!",
                            onClick = {
                                FightEvent.ResetHealthEvent
                                navController.navigate(route = "HomeScreen")
                            },
                            BoxColorCondition = false,
                        )
                    }
                }
                /*********************      Animation Block End       ********************/
            }
            Box(
                modifier = Modifier
                    .height(280.dp)
                    .fillMaxWidth()
                    .background(Color.Red)
            )
            {
                Row() {
                    Column(
                        modifier = Modifier
                            .fillMaxHeight()
                            .width(100.dp),
                    ) {
                        MenuButtonsFightScreen(
                            textvariable = "Attack",
                            onClickvariable = { viewModel.onEvent(FightEvent.AttackClickEvent) },
                            buttonImageid = R.drawable.choosebutton,
                            activeButton = state.value.myAbilityState == AbilityState.ATTACK
                        )
                        MenuButtonsFightScreen(
                            textvariable = "Magic",
                            onClickvariable = { viewModel.onEvent(FightEvent.MagicClickEvent) },
                            buttonImageid = R.drawable.choosebutton,
                            activeButton = state.value.myAbilityState == AbilityState.MAGIC
                        )
                        MenuButtonsFightScreen(
                            textvariable = "Party",
                            onClickvariable = { viewModel.onEvent(FightEvent.PartyClickEvent) },
                            buttonImageid = R.drawable.choosebutton,
                            activeButton = state.value.myAbilityState == AbilityState.PARTY
                            //das ist ein if statement, welches true ist wenn dieses eintrifft, da Party im AbilityState
                            // am Anfang aktiv ist, ist diese gleich deaktiviert.
                        )
                        MenuButtonsFightScreen(
                            textvariable = "Defend",
                            onClickvariable = { viewModel.onEvent(FightEvent.DefendClickEvent) },
                            buttonImageid = R.drawable.choosebutton
                        )
                    }
                    ActionMenu(
                        action = state.value.myAbilityState,

                        onAttackPerformed2 = { viewModel.onEvent(FightEvent.AttackEvent(it)) },

                        onMagicPerformed2 = { viewModel.onEvent(FightEvent.MagicEvent(it)) },

                        onPartySwitched2 = { viewModel.onEvent(FightEvent.PartyEvent(it)) },

                        klicksperre = state.value.klicksperre,

                        activeFia3Monster = state.value.monster,

                        partyState = state.value.partyState,

                        )
                }
            }
            LifeBar(
                state.value.monster.currentHealth,
                state.value.monster.maxHealth,
                modifier = Modifier
                    .fillMaxWidth()
            )
            Row() {
                state.value.monster.getImageID()?.let { painterResource(id = it) }
                    ?.let {
                        Image(
                            painter = it,
                            contentDescription = "Monster",
                            contentScale = ContentScale.Fit
                        )
                    }
                Column() {
                    Text(
                        text = "Name: ${state.value.monster.name}"
                    )
                    Text(
                        text = "Life: ${state.value.monster.currentHealth} / ${state.value.monster.maxHealth}",
                        modifier = Modifier
                            .background(Color.White)
                    )
                }
            }
//            Text(text = "Life: ${state.value.monster.name}")
        }
    }
}


@Composable
fun ActionMenu(
    action: AbilityState,
    onAttackPerformed2: (Attack) -> Unit,
    onMagicPerformed2: (Attack) -> Unit,
    onPartySwitched2: (Fia3Monster) -> Unit,
    activeFia3Monster: Fia3Monster,
    klicksperre: Boolean,
    partyState: Party,
) {
    when (action) {
        AbilityState.ATTACK -> {
            AttackMenu(
                onAttackPerformed = { onAttackPerformed2(it) },
                klicksperre = klicksperre,
                activeFia3Monster = activeFia3Monster,
            )
        }

        AbilityState.MAGIC ->
            MagicMenu(
                onMagicPerformed = { onMagicPerformed2(it) },
                klicksperre = klicksperre,
                activeFia3Monster = activeFia3Monster,
            )

        AbilityState.PARTY ->
            PartyMenu(
                onPartySwitched = { onPartySwitched2(it) },
                klicksperre = klicksperre,
                partyState = partyState
            )
    }
}


//@Composable
//fun AnimationPlay(
//
//) {
//    AnimatedVisibility(visibleState = klicksperre) {
//
//    }
//
//}


//fun PartyMenu(
//    onAttackPerformed: (Attack) -> Unit,
//    klicksperre: Boolean,
//    activeFia3Monster: Fia3Monster,


//    Button(modifier = Modifier
//        .clickable { <onClickvariable() > }
//        .height(50.dp)
//        .width(100.dp)
//        .background(Color.White)
//    ) {
//        Text(text = textvariable)
//        Image(
//            painter = painterResource(buttonImageid),
//            contentDescription = "Boss Button",
//            modifier = Modifier
//                .height(100.dp)
//                .width(200.dp),
//            contentScale = ContentScale.Crop
//        )


//@Composable
//fun MySliderDemo(
//    modifier:Modifier = Modifier,
//    life: Int,
//    lifeMax: Int,
//    Steps: Int,
//    color: Color,
//) {
//    var sliderPosition by remember { mutableStateOf(boss1.currentHealth) }
//    Text(text = sliderPosition.toString())
//    Slider(value = sliderPosition, onValueChange = { sliderPosition = it })
//}


//val resourceId = viewModel.getResourceID(imageResource)

//@Composable
//fun DamageDone(){
//    var doneDamage = Attack()
//    boss1.currentHealth =- this.Attack.dmg
//    Text(text = "${Fia3Monster} has taken $doneDamage damage.")
//}