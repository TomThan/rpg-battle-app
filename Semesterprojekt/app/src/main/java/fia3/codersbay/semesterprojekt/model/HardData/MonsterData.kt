package fia3.codersbay.semesterprojekt.model

val olli = Fia3Monster("Bulliver", oliattacks, olimagic, 20500, 20500)
val bartech = Fia3Monster("Bärtech", bartechattack, bartechmagic, 20500, 20500)
val oliver = Fia3Monster("Oliver Atom",oliattacks, olimagic, 20500, 20500)
val ati = Fia3Monster("Ati The Hen", attiattacks, attimagic, 20500, 20500)
val flohx = Fia3Monster("Fox McKaut", floattacks , flomagic, 20500, 20500)
val pig5 = Fia3Monster("Porte5k", listOf<Attack>(pipi, pipu, pipa, popelangriff), magicList, 500, 500)


val alex = Fia3Monster("Alex, Kotlin Dragon", alexList1, bossattacks2,10000,10000)
val alex2 = Fia3Monster("HydrAlex", alexList2, bossattacks2, 4999,10000)

val stefan = Fia3Monster("Sehtim, The Mean Steam Machine",stefanList1, bossattacks2, 10000,10000)
val stefan2 = Fia3Monster("Sehtim, The Alpha and The Omega", stefanList2, bossattacks2, 4999, 10000)

val roman = Fia3Monster("Romantischer Platz", romanList1, bossattacks2, 10000, 10000)
val roman2 = Fia3Monster("Roman, Master Exposer", romanList2, bossattacks2, 4999, 10000)



val boss1 = Fia3Monster(
    name = "Boss",
    bossattacks,
    bossattacks2,
    4000,
    4000,
)

val boss2 = Fia3Monster(
    name = "Boss2",
    bossattacks2,
    bossattacks,
    3000,
    5000,
)

val partyTestparty = Party(partyMemberList =
listOf(
    olli.copy(),
    boss1.copy(),
    oliver.copy(),
    ati.copy(),
    flohx.copy(),
    pig5.copy(),
    boss2.copy()
)
)