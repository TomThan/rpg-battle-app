package fia3.codersbay.semesterprojekt.database.model.dao

import fia3.codersbay.semesterprojekt.database.model.schema.Parties
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class PartySlot(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<PartySlot>(Parties)

    val slot = Int
    val Fia3Monster by Parties.Fia3Monster

}