package fia3.codersbay.semesterprojekt.model

import fia3.codersbay.semesterprojekt.R


val imageMapofAttack = mapOf<String, Int>(
    "pipipi" to R.drawable.baum_2,
    "Fire" to R.drawable.fire,
    "Kratzen" to R.drawable.scratch,
    "Bieber" to R.drawable.bieber,
    "Bibelaa" to R.drawable.tiger_4_wild,
    "Portek" to R.drawable.tiger_4_wild,
    "Blizzard" to R.drawable.blizzard,
    "Fabel" to R.drawable.tiger_4_wild,


    "Monster Energy" to R.drawable.monsterw,
    "Epic Seven" to R.drawable.epic7,
//Bartech Attacks
    "pipiTSD" to R.drawable.pipi,
    "Error Code 1337" to R.drawable.stack,
    "Beste Marke" to R.drawable.ja,
    "Log-in" to R.drawable.login,
    //Bartech Magic
    "Monster Energy aus Polen" to R.drawable.monsterp,
    "Polish Genes" to R.drawable.polgirl,
    "Polish Names" to R.drawable.gregor,

        //Atom Attacks
        "Top Spin" to R.drawable.topspin,
        "McFit Combo" to R.drawable.mcfit,
        "Persona 3" to R.drawable.mitsuru,
        "Nerd" to R.drawable.nerd,

        //Atom Magic
        "Mr Morj" to R.drawable.morj,
        "Will of D" to R.drawable.will,

        //Atti Hen
        "Shit" to R.drawable.schade,
        "But" to R.drawable.but,
        "FIA2" to R.drawable.fia2,

        "ein T" to R.drawable.eint,


    //FoxMcKaut
    "Flip Kick" to R.drawable.upsmash,
    "Fox Split" to R.drawable.downsmash,
    "Roundhouse Kick" to R.drawable.fsmash,
    "Taunt" to R.drawable.taunt,
    "Fox Fire" to R.drawable.upb,
    "Reflector" to R.drawable.shine,
    "Blaster" to R.drawable.neutralb,

    //Alex Boss
    "Hoisting" to R.drawable.hoist,
    "Neue Klasse" to R.drawable.horse,
    "David" to R.drawable.angaben,

    "Urlaub" to R.drawable.ireland,
    "Lambda" to R.drawable.lambda,
    "insert" to R.drawable.insert,

    //Stefan Boss
    "deflect" to R.drawable.angaben,
    "nlogn" to R.drawable.nlogn,
    "Ant" to R.drawable.ant,
    "Rekursion" to R.drawable.rekursion,


    //Roman Boss
    "Not Avaiable" to R.drawable.nothing,
    "Delegate" to R.drawable.delegate,
    "Exposed" to R.drawable.exposed,
    "Compose" to R.drawable.compose,


    )

val imageloaderMap = mapOf<String, Int>(
    "Boss" to R.drawable.tiger_4_wild,
    "Bulliver" to R.drawable.baum_1,
    "Portek" to R.drawable.baum_2,
    "Boss2" to R.drawable.bieber,
    "Alex, Kotlin Dragon" to R.drawable.alex1,
    "HydrAlex" to R.drawable.alex2,
    "Sehtim, The Mean Steam Machine" to R.drawable.stefan,
    "Sehtim, The Alpha and The Omega" to R.drawable.stefan2,
    "Romantischer Platz" to R.drawable.roman1,
    "Roman, Master Exposer" to R.drawable.roman2,
    "Fox McKaut" to R.drawable.fox,
    "Oliver Atom" to R.drawable.oli,
    "Bärtech" to R.drawable.bartek,
    "Ati The Hen" to R.drawable.ati,

    )




