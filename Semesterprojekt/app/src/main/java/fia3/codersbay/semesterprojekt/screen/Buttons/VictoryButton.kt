package fia3.codersbay.semesterprojekt.screen.Buttons

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun VictoryButton(
    text: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    BoxColorCondition: Boolean = true,
) {
    Box(

        modifier = modifier
            .padding(start = 50.dp, top = 50.dp)
            .clickable { onClick() }
            .width(width = 300.dp)
            .height(height = 300.dp)
            .background(if (BoxColorCondition) Color.Blue else Color.Red),
    ) {
        Column() {
            Text(
                text = text,
                color = Color(0xfff5f5f5),
                style = TextStyle(
                    fontSize = 30.sp,
                    fontWeight = FontWeight.Bold
                ),
                modifier = Modifier
                    .width(width = 300.dp)
                    .height(height = 200.dp)
            )
            Text(
                text = "Continue?",
                color = Color(0xfff5f5f5),
                style = TextStyle(
                    fontSize = 34.sp,
                    fontWeight = FontWeight.Bold
                ),
                modifier = Modifier
                    .width(width = 300.dp)
                    .height(height = 100.dp),
                textAlign = TextAlign.Center
            )
        }
    }
}
