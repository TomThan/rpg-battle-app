package fia3.codersbay.semesterprojekt.screen

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.lifecycle.viewmodel.compose.viewModel
import fia3.codersbay.semesterprojekt.event.PartyEditEvent
import fia3.codersbay.semesterprojekt.model.Fia3Monster
import fia3.codersbay.semesterprojekt.model.Party
import fia3.codersbay.semesterprojekt.model.partyTestparty
import fia3.codersbay.semesterprojekt.screen.Buttons.PartyEditTeam
import fia3.codersbay.semesterprojekt.screen.Buttons.PartyMenu
import fia3.codersbay.semesterprojekt.viewmodel.PartyEditViewModel


@Composable
fun PartyEditScreen(
    navController: NavController,
    viewModel: PartyEditViewModel = viewModel(),
    navigateTo: () -> Unit,

    ) {


    val state = viewModel.state



    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Blue),
    )
    {
        Column() {

            Row()
            {
                Box(
                    modifier = Modifier
                        .height(60.dp)
                        .fillMaxWidth()
                        .background(Color.Green)
                ) {
                    Box(modifier = Modifier
                        .size(50.dp)
                        .padding(start = 5.dp, top = 5.dp)
                        .background(Color.Red)
                        .clickable { navController.navigate(route = "HomeScreen") }
                    )
                    Box(
                        modifier = Modifier
                            .height(50.dp)
                            .width(300.dp)
                            .padding(start = 100.dp, top = 5.dp)
                            .background(Color.Yellow)
                    ) {
                        Text(text = "Party", color = Color.Red)
                    }
                }
            }
            Box(
                modifier = Modifier
                    .height(400.dp)
                    .width(400.dp)
                    .padding(start = 30.dp, top = 150.dp, end = 30.dp, bottom = 50.dp)
                    .background(Color.Cyan)
            )
            {
                Box(
                    modifier = Modifier
                        .height(50.dp)
                        .fillMaxWidth()
                        .background(Color.Green)
                )
                {
                    Text(text = "Team Composition", color = Color.Red)
                }
                for (index in 0..2) {
                    Box(
                        modifier = Modifier
                            .height(200.dp)
                            .width(400.dp)
                            .padding(top = 50.dp)
                            .background(Color.Yellow)
                    ) {

//                        SelectedParty(
//                            party = state.value.partyState,
//                            partyIndex = 4,
//                            klicksperre = state.value.klicksperre,
//                            onClick = { viewModel.onEvent2(PartyEditEvent.PartyEvent2(it)) }
//                        )

                        PartyEditTeam(
                            onPartySwitched = { viewModel.onEvent2(PartyEditEvent.PartyEvent2(it)) },
                            klicksperre = state.value.klicksperre,
                            partyState = state.value.partyState
                        )

                    }
                }
            }

            Box(
                modifier = Modifier
                    .background(Color.Red)
                    .padding(top = 150.dp)
                    .fillMaxSize()
            ) {
                PartyEditMenu(
                    onPartySwitched2 = { viewModel.onEvent2(PartyEditEvent.PartyEvent(it)) },
                    klicksperre = state.value.klicksperre,
                    partyState = partyTestparty
                )
                LazyVerticalGrid(
                    columns = GridCells.Fixed(2), contentPadding = PaddingValues(top = 20.dp)
                ) {

                }
            }
        }
    }
}

@Composable
fun PartyEditMenu(
    onPartySwitched2: (Fia3Monster) -> Unit,
    klicksperre: Boolean,
    partyState: Party,
) {
    PartyMenu(
        onPartySwitched = { onPartySwitched2(it) },
        klicksperre = klicksperre,
        partyState = partyState
    )
}