package fia3.codersbay.semesterprojekt.model

enum class AbilityState{
    MAGIC, PARTY, ATTACK,
}