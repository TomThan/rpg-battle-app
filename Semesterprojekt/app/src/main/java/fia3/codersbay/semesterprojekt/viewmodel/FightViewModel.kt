package fia3.codersbay.semesterprojekt.viewmodel

import android.util.Log
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fia3.codersbay.semesterprojekt.database.model.dao.FightCounter
import fia3.codersbay.semesterprojekt.database.model.schema.FightCounters
import fia3.codersbay.semesterprojekt.event.FightEvent
import fia3.codersbay.semesterprojekt.model.AbilityState
import fia3.codersbay.semesterprojekt.model.Attack
import fia3.codersbay.semesterprojekt.model.Fia3Monster
import fia3.codersbay.semesterprojekt.model.Party
import fia3.codersbay.semesterprojekt.model.*
import fia3.codersbay.semesterprojekt.model.boss1
import fia3.codersbay.semesterprojekt.model.bossattacks
import fia3.codersbay.semesterprojekt.model.olli
import fia3.codersbay.semesterprojekt.model.bartech
import fia3.codersbay.semesterprojekt.states.FightState
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jetbrains.exposed.sql.transactions.transaction

class FightViewModel(savedStateHandle: SavedStateHandle) : ViewModel() {

    private var level: Int? = savedStateHandle.get<Int>("level")
    private var levelAlreadyReached: Boolean = false

    var _state = mutableStateOf(FightState())
    val state: State<FightState> = _state

    init {
        if (level != null) {
            _state.value = state.value.copy(
                monster = state.value.partyState.partyMemberList[0],
                enemyMonster = when (level) {
                    1 -> alex.copy()
                    3 -> stefan.copy()
                    else -> roman.copy()
                }
            )
        }
    }


    fun onEvent(event: FightEvent) {
        if (!state.value.klicksperre) {
            when (event) {

                is FightEvent.AttackClickEvent -> {
                    _state.value = state.value.copy(myAbilityState = AbilityState.ATTACK)
                }

                is FightEvent.MagicClickEvent -> {
                    _state.value = state.value.copy(myAbilityState = AbilityState.MAGIC)
                }

                is FightEvent.PartyClickEvent -> {
                    _state.value = state.value.copy(
                        myAbilityState = AbilityState.PARTY
                    )
                }

                is FightEvent.ResetHealthEvent -> {
                    resetHealth()
                }

                is FightEvent.AttackEvent -> {
                    Log.d("Angriff oida", "${event.attack}")
                    actionperformed(event.attack)


                }

                is FightEvent.MagicEvent -> {
                    Log.d("Magic oida", "${event.magic}")
                    actionperformed(event.magic)
                }

                is FightEvent.PartyEvent -> {
                    partyswitch(event.clickedPartyMember)
                }

                is FightEvent.DefendClickEvent -> {
                    defend()
                }
            }
        }
    }


    fun actionperformed(attack: Attack) {
        val enemytakendamage = attack.dmg.random()
        val indexOfPartyMember: Int

        if (attack.dmg.random() < 0) {
//            _state.value = state.value.copy(
//                monster = state.value.monster.copy(
//                    currentHealth = state.value.monster.currentHealth - enemytakendamage,
//                )
//            )
            val listOfActiveParty = state.value.partyState.partyMemberList

            indexOfPartyMember = listOfActiveParty.indexOf(state.value.monster)
            listOfActiveParty[indexOfPartyMember].currentHealth =
                listOfActiveParty[indexOfPartyMember].currentHealth - enemytakendamage
            if (listOfActiveParty[indexOfPartyMember].currentHealth > listOfActiveParty[indexOfPartyMember].maxHealth) {
                listOfActiveParty[indexOfPartyMember].currentHealth =
                    listOfActiveParty[indexOfPartyMember].maxHealth
            }

            _state.value = state.value.copy(
                partyState = state.value.partyState.copy(
                    partyMemberList = listOfActiveParty
                ),
                monster = listOfActiveParty[indexOfPartyMember],
            )
//            if (_state.value.monster.currentHealth > _state.value.monster.maxHealth) {
////                _state.value.monster.currentHealth = _state.value.monster.maxHealth
//
//                _state.value = state.value.copy(
//                    monster = state.value.monster.copy(
//                        currentHealth = state.value.monster.maxHealth,
//                    )
//                )
//
//            }
        } else {
            _state.value = state.value.copy(
                enemytakendamage = enemytakendamage,
                enemyMonster = state.value.enemyMonster.copy(
                    currentHealth = state.value.enemyMonster.currentHealth - enemytakendamage,
                )
            )
        }

        viewModelScope.launch {
            _state.value = state.value.copy(
                klicksperre = true,
                attackAnimation = true,
                attackUsed = attack
            )
            delay(1000)
            _state.value = state.value.copy(
                enemytakendamage = null,
                attackAnimation = false
            )
            delay(500)
            if (state.value.enemyMonster.currentHealth > 0) {
                if ((state.value.enemyMonster.currentHealth < (_state.value.enemyMonster.maxHealth / 2)
                            && level != null) && !levelAlreadyReached
                ) {
                    level = level!! + 1
                    Log.d("Level", "$level Level increased by 1")
                    state.value.enemyMonster = when (level) {
                        2 -> alex2.copy(currentHealth = state.value.enemyMonster.currentHealth)
                        4 -> stefan2.copy(currentHealth = state.value.enemyMonster.currentHealth)
                        else -> roman2.copy(currentHealth = state.value.enemyMonster.currentHealth)
                    }
                    levelAlreadyReached = true
                }

                attackReceived(state.value.enemyMonster.AttackNames.random())

                if (state.value.monster.currentHealth <= 0) {
                    _state.value = state.value.copy(
                        defeatState = true,
                        klicksperre = true,
                    )
                }
            } else {
                delay(500)
                Thread.sleep(500)
                _state.value = state.value.copy(
                    victoryState = true,
                    klicksperre = true,
                )
                transaction {
                    FightCounter
                        .find { FightCounters.name eq "Battles done" }.first()
                        .Counter++
                }
            }
        }
    }

    fun resetHealth() {
        val newParty = Party(partyMemberList = listOf(bartech.copy(), olli.copy(), boss1.copy()))

        _state.value = state.value.copy(
            partyState = newParty,
            monster = newParty.partyMemberList[0],
            enemyMonster = when (level) {
                2 -> alex2.copy()
                4 -> stefan2.copy()
                else -> roman2.copy()
            }
        )
    }

    fun partyswitch(partyMemberClicked: Fia3Monster) {

        _state.value = state.value.copy(
            monster = partyMemberClicked
        )

//        if(state.enemyMonster.currentHealth <= 0) { soll in der View sein
//
//        }
    }

    fun defend() {
        state.value.defendState = true
        viewModelScope.launch {
            delay(500)
            attackReceived(bossattacks.random())
        }
        Log.d("pipi", "pipi")

    }

    fun enemyAttack(attack: Attack) {
        Log.d("pipi", "pipi")
        attackReceived(bossattacks.random())
    }

    fun attackReceived(attack: Attack) {
        val takendamage = attack.dmg.random()

        if (state.value.defendState == false) {
            state.value.monster.currentHealth -= takendamage
            _state.value = state.value.copy(
                takendamage = takendamage,
                bossattackUsed = attack,
                enemyAttackAnimation = true,
            )
            viewModelScope.launch {
                delay(500)
                _state.value = state.value.copy(
                    takendamage = null,
                    klicksperre = false,
                    enemyAttackAnimation = false,
                )
            }
        } else {
            state.value.monster.currentHealth -= takendamage / 50
            _state.value = state.value.copy(
                takendamage = takendamage,
                bossattackUsed = attack,
                enemyAttackAnimation = true,
            )
            viewModelScope.launch {
                delay(500)
                _state.value = state.value.copy(
                    takendamage = null,
                    klicksperre = false,
                    enemyAttackAnimation = false,
                    defendState = false
                )
            }
        }
    }
//Create state for my Monster with testmonster as value
//    val Monster:Fia3Monster("Pig", <"Fire",200>
//    )


//    var attacks by mutableStateOf<Attack>(Attack(it,it))
//
//    var magicState by mutableStateOf<MagicState>(AbilityState.MAGIC)

//    fun getResourceID(imageResource: String): Int? { //? heißt Nullable :Int == Rückgabewert
//        val drawableResource = application.baseContext.resources
//            .getIdentifier(
//                imageResource,
//                "drawable",
//                application.baseContext.packageName
//            )
//        return if (drawableResource == 0) null else drawableResource
//    }


}


