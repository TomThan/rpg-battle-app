package fia3.codersbay.semesterprojekt.database.model.schema

import org.jetbrains.exposed.dao.id.IntIdTable

object Attacks : IntIdTable() {
    val name = varchar("name",255)
    val min_dmg = integer("min_dmg")
    val max_dmg = integer("max_dmg")
    val isMagic = bool("isMagic")
}