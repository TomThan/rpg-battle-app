package fia3.codersbay.semesterprojekt.event

import fia3.codersbay.semesterprojekt.model.Attack
import fia3.codersbay.semesterprojekt.model.Fia3Monster



sealed class FightEvent {
    object AttackClickEvent : FightEvent()
    object MagicClickEvent : FightEvent()
    object PartyClickEvent : FightEvent()
    object DefendClickEvent : FightEvent()

    object ResetHealthEvent : FightEvent()

    data class AttackEvent(val attack: Attack) : FightEvent()
    data class MagicEvent(val magic: Attack) : FightEvent()
    data class PartyEvent(val clickedPartyMember: Fia3Monster) : FightEvent()

}