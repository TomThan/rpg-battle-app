package fia3.codersbay.semesterprojekt.viewmodel

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import fia3.codersbay.semesterprojekt.states.FightState

class HomeViewModel() : ViewModel() {


    var _state = mutableStateOf(FightState())
    val state: State<FightState> = _state

}


