package fia3.codersbay.semesterprojekt.viewmodel

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fia3.codersbay.semesterprojekt.database.model.dao.PartySlot
import fia3.codersbay.semesterprojekt.database.model.schema.PartySlots
import fia3.codersbay.semesterprojekt.event.PartyEditEvent
import fia3.codersbay.semesterprojekt.states.FightState
import fia3.codersbay.semesterprojekt.model.Fia3Monster
import kotlinx.coroutines.launch
import org.jetbrains.exposed.sql.transactions.transaction

class PartyEditViewModel() : ViewModel() {


    var _state = mutableStateOf(FightState())
    val state: State<FightState> = _state

//    var eventofParty = mutableStateOf(PartyEditEvent.PartyEventtoDB(PartySlots()))
//
//    private fun stateToDB(block: PartyEditEvent.PartyEventtoDB.() -> Unit) {
//        viewModelScope.launch {
//            transaction {
//                eventofParty.value.copy(
//                  eventofParty =
//                )
//                = event
//            }
//        }
//    }

    fun onEvent2(event: PartyEditEvent) {
        if (!state.value.klicksperre) {
            when (event) {

                is PartyEditEvent.PartyEvent -> {
                    partyAdd(event.clickedPartyMember)
                }

                is PartyEditEvent.PartyEvent2 -> {
                    partyRemove(event.clickedPartyMember)
                }

                else -> {
                    println("d")
                }
            }
        }
    }
    fun partyRemove(memberOfPartyClicked: Fia3Monster) {

        _state.value = state.value.copy(
            partyState = state.value.partyState.copy(
                partyMemberList = state.value.partyState.partyMemberList.minus(memberOfPartyClicked)

            )

        )
    }

    fun partyAdd(memberOfPartyClicked: Fia3Monster) {
        if (state.value.partyState.partyMemberList.contains(memberOfPartyClicked)) {
            _state.value = state.value.copy(
                partyState = state.value.partyState.copy(
                    partyMemberList = state.value.partyState.partyMemberList.minus(
                        memberOfPartyClicked
                    )

                )
            )
        } else if (state.value.partyState.partyMemberList.lastIndex < 3)
            _state.value = state.value.copy(
                partyState = state.value.partyState.copy(
                    partyMemberList = state.value.partyState.partyMemberList.plus(
                        memberOfPartyClicked
                    )

                )
            )
    }
}




