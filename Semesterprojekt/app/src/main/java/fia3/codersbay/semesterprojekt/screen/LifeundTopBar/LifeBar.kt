package fia3.codersbay.semesterprojekt.screen.LifeundTopBar

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun LifeBar(life: Int, MaxLife: Int, modifier: Modifier = Modifier ) {
    Box(
        modifier = modifier
            .height(30.dp)
            .width(300.dp)
            .border(width = 2.dp, color = Color.Black)
            .background(Color.Red)
    ) {
        Box(
            modifier = Modifier
                .height(30.dp)
                .fillMaxWidth((life / MaxLife.toFloat()))
                .background(Color.Green)
        )
        {
            Text(text = " Life: $life")
        }
    }
}