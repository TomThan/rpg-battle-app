package fia3.codersbay.semesterprojekt.database.model.schema

import org.jetbrains.exposed.dao.UUIDEntity
import org.jetbrains.exposed.dao.UUIDEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import java.util.UUID

class Fia3Monster(id: EntityID<UUID>) : UUIDEntity(id) {
//    companion object : UUIDEntityClass<AttacksDAO>(Attacks)

    var name by Fia3monster.name
}