package fia3.codersbay.semesterprojekt.screen.LifeundTopBar

import android.webkit.WebSettings
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import java.time.format.TextStyle

@Composable
fun BigBlackTopBar(text: String) {
    Text(
        text = text,
        modifier = Modifier
        .padding(start = 55.dp, top = 5.dp),
    color = Color.Magenta, textAlign = TextAlign.Justify, fontSize = 22.sp
    )
}

@Composable
fun BigBlackMiddleBar(text: String) {
    Text(
        text = text,
        modifier = Modifier
            .padding(start = 50.dp, top = 240.dp),
        color = Color.Magenta, textAlign = TextAlign.Justify, fontSize = 22.sp
    )
}
